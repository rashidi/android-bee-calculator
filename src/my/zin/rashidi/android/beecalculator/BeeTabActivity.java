/**
 * 
 */
package my.zin.rashidi.android.beecalculator;

import static android.view.LayoutInflater.from;
import static my.zin.rashidi.android.DialogHelper.alertDialog;

import java.util.HashMap;
import java.util.Map;

import my.zin.rashidi.android.R;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

/**
 * @author Rashidi Zin
 * @version 1.0
 * @since 1.0
 * 
 * Based on tutorial from http://code.google.com/p/android-custom-tabs/
 */
public class BeeTabActivity extends TabActivity {
	
	private TabHost tabHost;
	
	Map<String, Class<? extends Activity>> tabContent;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	
		setTabHost();
		tabHost.getTabWidget().setDividerDrawable(android.R.drawable.divider_horizontal_dark);
		
		for (String keySet : tabContent.keySet()) {
			setupTab(new TextView(this), keySet, tabContent.get(keySet));
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
	
		case R.id.itemAbout:
			alertDialog(this, getString(R.string.about_content)).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}		
	}
	
	private void setTabContent() {
		tabContent = new HashMap<String, Class<? extends Activity>>();
		
		tabContent.put(getString(R.string.calculate), EnergyActivity.class);
	}
	
	private void setTabHost() {
		setTabContent();
	
		tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabHost.setup();
	}

	private void setupTab(final View view, final String label, final Class<? extends Activity> clazz) {
		Intent intent = new Intent().setClass(this, clazz);
		View tabView = createTabView(tabHost.getContext() , label);
		TabSpec content = tabHost.newTabSpec(label).setIndicator(tabView).setContent(intent);
		
		tabHost.addTab(content);
	}
	
	private View createTabView(final Context context, final String text) {
		View view = from(context).inflate(R.layout.tabs_bg, null);
		
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		
		return view;
	}
}
