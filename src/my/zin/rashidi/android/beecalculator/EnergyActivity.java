/**
 * 
 */
package my.zin.rashidi.android.beecalculator;

import static java.lang.Double.parseDouble;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static my.zin.rashidi.android.DialogHelper.alertDialog;

import java.text.DecimalFormat;

import my.zin.rashidi.android.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

/**
 * @author Rashidi Zin
 * @version 1.0
 * @since 1.0
 */
public class EnergyActivity extends Activity {

	private Button btnEnergyRequirement;
	private double bee;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_energy);
		
		btnEnergyRequirement = (Button) findViewById(R.id.btnEnergyRequirement);
		btnEnergyRequirement.setEnabled(false);
		
		((EditText) findViewById(R.id.editTextBee)).setKeyListener(null);
	}

	public void onButtonClicked(View view) {
		Button button = (Button) view;
		String text = button.getText().toString();
		
		try {
			final double age = parseDouble(((EditText) findViewById(R.id.editAge)).getText().toString());
			final double weight = parseDouble(((EditText) findViewById(R.id.editWeight)).getText().toString());
			final double height = parseDouble(((EditText) findViewById(R.id.editHeight)).getText().toString());
			
			int gender = ((RadioGroup) findViewById(R.id.radioGroupGender)).getCheckedRadioButtonId();
			
			bee = 0.0;
			
			if (getString(R.string.benedict).equals(text)) {
				bee = getByBenedict(weight, height, age, gender);
			} else {
				bee = getByMiffin(weight, height, age, gender);
			}
			
			DecimalFormat decimalFormat = new DecimalFormat("#.##");
			bee = Double.valueOf(decimalFormat.format(bee));
			
			((EditText) findViewById(R.id.editTextBee)).setText(valueOf(bee));
			
			btnEnergyRequirement.setEnabled(true);
			
		} catch (NumberFormatException nfe) {
			alertDialog(
					this, 
					format(
							"%s, %s, and %s are required", 
							getString(R.string.age),
							getString(R.string.weight),
							getString(R.string.height)
					)).show();
		}
	}
	
	public void onButtonClearClicked(View view) {
		((EditText) findViewById(R.id.editAge)).setText("");
		((EditText) findViewById(R.id.editWeight)).setText("");
		((EditText) findViewById(R.id.editHeight)).setText("");
		((EditText) findViewById(R.id.editTextBee)).setText("");
		
		if (btnEnergyRequirement.isEnabled()) {
			btnEnergyRequirement.setEnabled(false);
		}
	}
	
	public void onBtnEnergyReqClicked(View view) {
		
		Intent intent = new Intent(getApplicationContext(), EneryRequirementActivity.class);
		intent.putExtra(getString(R.string.bee), this.bee);
		
		startActivity(intent);
	}
	
	/**
	 * 
	 * @param double weight in kg
	 * @param double height in cm
	 * @param double age in years
	 * @param int gender
	 * @return
	 */
	private double getByBenedict(double weight, double height, double age, int gender) {
		
		if (gender == R.id.radio_male) {
			return (66.47 + (13.75 * weight) + (5 * height)) - (6.76 * age);
		} else {
			return (655.1 + (9.56 * weight) + (1.85 * height)) - (4.68 * age); 
		}
	}

	/**
	 * 
	 * @param double weight in kg
	 * @param double height in cm
	 * @param double age in years
	 * @param int gender
	 * @return
	 */
	private double getByMiffin(double weight, double height, double age, int gender) {
		
		if (gender == R.id.radio_male) {
			return (10 * weight) + (6.25 * height) - (5 * age) + 5;
		} else {
			return (10 * weight) + (6.25 * height) - (5 * age) - 161;
		}
	}
	
}
