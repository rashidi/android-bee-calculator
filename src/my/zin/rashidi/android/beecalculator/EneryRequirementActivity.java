/**
 * 
 */
package my.zin.rashidi.android.beecalculator;

import static java.lang.Double.parseDouble;
import static java.lang.String.format;
import static my.zin.rashidi.android.DialogHelper.alertDialog;

import java.text.DecimalFormat;

import my.zin.rashidi.android.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * @author shidi
 * @version 1.0
 * @since 1.0
 */
public class EneryRequirementActivity extends Activity {

	private double bee = 0.0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_energy_requirement);
		
		setBeeValue();
		((EditText) findViewById(R.id.editEnergyReq)).setKeyListener(null);
	}
	
	public void onButtonClearClicked(View view) {
		((EditText) findViewById(R.id.editActivity)).setText("");
		((EditText) findViewById(R.id.editStress)).setText("");
		((EditText) findViewById(R.id.editEnergyReq)).setText("");
	}
	
	public void onButtonCalcEnergyReqClicked(View view) {
		try {
			final double activity = parseDouble(((EditText) findViewById(R.id.editActivity)).getText().toString());
			final double stress = parseDouble(((EditText) findViewById(R.id.editStress)).getText().toString());
			
			final double energyReq = bee * activity * stress;
			DecimalFormat dF = new DecimalFormat("#.##");
			
			((EditText) findViewById(R.id.editEnergyReq)).setText(dF.format(energyReq));
		} catch (NumberFormatException nfe) {
			alertDialog(
					this, 
					format(
							"%s and %s are required", 
							getString(R.string.activity),
							getString(R.string.stress)
					)).show();
		}
	}
	
	
	private void setBeeValue() {
		Intent intent = getIntent();
		bee = intent.getDoubleExtra(getString(R.string.bee), 0.0);
		
		EditText editDefaultBee = (EditText) findViewById(R.id.editDefaultBee);
		
		editDefaultBee.setText(String.valueOf(bee));
		editDefaultBee.setKeyListener(null);
	}

}
