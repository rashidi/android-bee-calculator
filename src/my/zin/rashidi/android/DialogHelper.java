/**
 * 
 */
package my.zin.rashidi.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * @author Rashidi Zin
 * @version 1.0
 * @since 1.0
 */
public class DialogHelper {

	public DialogHelper() { }
	
	public static AlertDialog alertDialog(Context context, String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg).setCancelable(false).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
			
		});
		
		return builder.create();
	}

}
